#!/bin/sh

# setup fusesoc
export fusesoc_tmp=/tmp/fusesoc
[ -e $fusesoc_tmp ] && rm -rf $fusesoc_tmp
mkdir -p $fusesoc_tmp/fusesoc_libraries
touch $fusesoc_tmp/fusesoc.conf
alias fs='fusesoc --config $fusesoc_tmp/fusesoc.conf'

# add ip_lib library
fs library add --location $fusesoc_tmp/fusesoc_libraries/ip_lib --sync-type git ip_lib https://gitlab.com/ip-vhdl/ip_lib
fs library update
fs library list

# check ip core
fs core list
fs core show mux_fifo

# run

# necessary, as hard coded in Makefile
cd /tmp

# Reproduce here tests in ci
fs run --target=simulate.run.uniform.complex --tool=generic mux_fifo
