#!/bin/sh

# restart from scratch, just in case
# make clean

# data producer is vhdl osvvm library
MIN_INTERVAL=50 MAX_INTERVAL=300 NB_CHANNELS=60 TIMEOUT=1000 COVERAGE_SAMPLES_OSVVM=1 make -f Makefile.mux_fifo
