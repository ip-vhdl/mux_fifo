# open project
open_project mux_fifo.xpr

reset_runs synth_1
reset_runs impl_1

# synthesis
launch_runs synth_1 -jobs 4
wait_on_run synth_1

# implement
launch_runs impl_1 -jobs 4
wait_on_run impl_1

# # set property
# open_run impl_1

# # set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
# set_property STEPS.WRITE_BITSTREAM.TCL.PRE {../../../pre-bit.tcl} [get_runs impl_1]

# # bitstream
# launch_runs impl_1 -to_step write_bitstream -jobs 40
